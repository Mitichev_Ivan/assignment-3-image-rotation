#include "bmp.h"
#include <stdio.h>
#include <stdlib.h>
#define PIXEL_SIZEOF sizeof(struct pixel)
#define BF_TYPE 0x4D42
#define BF_RESERVED 0
#define BI_SIZE 40
#define BI_PLANES 1
#define BI_BIT_COUNT 24
#define BI_COMPRESSION 0
#define BI_X_PIXELS_PER_M 0
#define BI_Y_PIXELS_PER_M 0
#define BI_COLORS_USED 0
#define BI_COLORS_IMPORTANT 0
int find_padding(uint32_t width) {
	return (int)(4 - (width * PIXEL_SIZEOF) % 4) % 4;
}
// Реализация функции чтения изображения из BMP файла
enum read_status from_bmp(FILE* in, struct image *img) {
    if (!in || !img) {
    free_image(img);
    return READ_OUT_OF_MEMORY;}
    struct bmp_header header;
    if (fread(&header, sizeof(header), 1, in) != 1) {
    free_image(img);
    return READ_INVALID_HEADER;}
    img->width = header.biWidth;
    img->height = header.biHeight;
    img->data = (struct pixel*) malloc(img->width * img->height * PIXEL_SIZEOF);
    if (!img->data) {
        return READ_OUT_OF_MEMORY;
    }
    for (size_t i = 0; i < img->height; i++) {
	if ((fread(img->data + i * img->width, PIXEL_SIZEOF, img->width, in) != img->width)){
		free_image(img);
		return READ_INVALID_PIXELS;}
		if (fseek(in, find_padding(img->width), SEEK_CUR)) {
			free_image(img);
			return READ_INVALID_PIXELS;
		}
	}
    
    
    return READ_OK;	
}
static struct bmp_header generate_header(const struct image* img){
    struct bmp_header header;
    uint32_t image_size = img->width * PIXEL_SIZEOF + (find_padding(img->width)) * img->height;
    header.bfType = BF_TYPE;
    header.bfileSize = sizeof(struct bmp_header)+image_size;
    header.bfReserved = BF_RESERVED;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = BI_SIZE;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = BI_PLANES;
    header.biBitCount = BI_BIT_COUNT;
    header.biCompression = BI_COMPRESSION;
    header.biSizeImage = image_size;
    header.biXPelsPerMeter = BI_X_PIXELS_PER_M;
    header.biYPelsPerMeter = BI_Y_PIXELS_PER_M;
    header.biClrUsed = BI_COLORS_USED;
    header.biClrImportant = BI_COLORS_IMPORTANT;
    return header;
}
enum write_status to_bmp(FILE* out, struct image* img) {
    struct bmp_header header=generate_header(img);
    
    if (!(fwrite(&header, sizeof(struct bmp_header),1,out)==1)) {
	return WRITE_HEADER_ERROR;}
    for (uint32_t i = 0; i < img->height; i++) {
   	if (fwrite(&img->data[i * img->width], PIXEL_SIZEOF, img->width, out) != img->width) {return WRITE_DATA_ERROR;}
	if (fseek(out, find_padding(img->width), SEEK_CUR)) {
            return WRITE_PADDING_ERROR;
        }
			
    }
    return WRITE_OK;

}

