#include "rotation.h"

static struct image* rotate_90(struct image* img) {
    struct image* result_image;
    result_image = init_image(img->height, img->width);
    if (result_image == NULL) {
        return NULL;
    }
    for (size_t i = 0; i < img->height; i++) { 
	for (size_t j = 0; j < img->width; j++) {
		result_image->data[(result_image->height-j-1)*result_image->width+i]=img->data[img->width*i+j];}
    }
    free_image(img);
    return result_image;
}
static struct image* rotate_180(struct image* img) {
    struct image* result_image;
    result_image = init_image(img->width, img->height);
    if (result_image == NULL) {
        return NULL;
    }
    for (size_t i = 0; i < img->height; i++) {
        for (size_t j = 0; j < img->width; j++) {
            result_image->data[(result_image->height - i - 1) * result_image->width + (result_image->width - j - 1)] = img->data[i * img->width + j];
        }
    }
    free_image(img);
    return result_image;
}
struct image* rotate(struct image* img, int angle) {
    if(img==NULL){
        return NULL;
    }
    struct image* result_image = img;
    switch (angle) {
    case 0:
	return result_image;
	break;
    case -270:
    case 90:
	result_image = rotate_90(result_image);
	return result_image;
	break;
    case -180:
    case 180:
	result_image = rotate_180(result_image);
	return result_image;
	break;
    case -90:
    case 270:
	result_image = rotate_180(result_image);
	result_image = rotate_90(result_image);
	return result_image;
	break;
    default:
	return result_image;
    }

}
