#include "image.h"
#include <stdio.h>
#include <stdlib.h>

struct image* init_image(uint64_t width, uint64_t height) {
    struct image* img = malloc(sizeof(struct image));
    if (img == NULL) {
        return NULL;
    }
    img->width = width;
    img->height = height;
    img->data = malloc(sizeof(struct pixel) * width * height);
    if (img->data == NULL) {
        free(img);
        return NULL;
    }
    return img;
}
void free_image(struct image* img) {
    free(img->data);
    free(img);
}
