#include "bmp.h"
#include "read_status.h"
#include "rotation.h"
#include "write_status.h"
#include <stdio.h>
#include <stdlib.h>

int main( int argc, char** argv ) {
    if (argc != 4) {
	perror("Неверное число аргументов");
	return 1;}

    int angle = atoi(argv[3]);
    if(angle % 90 != 0){
	perror("Неверный угол");
	return 1;}

    FILE* open_file = fopen(argv[1], "rb");
    if (!open_file) {
	perror("Невозможно открыть файл с таким именем\n");
	return 1;}

    struct image* img = malloc(sizeof(struct image));
    enum read_status read_st = from_bmp(open_file, img);
    if (read_st != READ_OK) {
	perror("Проблемы с чтением файла\n");
	free_image(img);
	return 1;}
    struct image* result_img = rotate(img, angle);
    if(result_img==NULL){
        perror("Проблемы с поворотом картинки\n");
        free_image(result_img);
        return 1;}
    FILE* out_file = fopen(argv[2], "wb");
    if (!out_file) {
	perror("Невозможно записать картинку в файл с таким именем\n");
    free_image(result_img);
	return 1;}
    enum write_status write_st = to_bmp(out_file, result_img);
    free_image(result_img);
    if (write_st != WRITE_OK) {
	perror("Проблемы с записью в файл\n");
	return 1;}
     return 0;
    
}
