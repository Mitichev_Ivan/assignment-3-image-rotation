#ifndef ROTATION_H
#define ROTATION_H
#include "image.h"
#include <stddef.h>
struct image* rotate(struct image* img, int angle);
#endif
