#ifndef WRITE_STATUS_H
#define WRITE_STATUS_H
#include "image.h"
#include <stdio.h>
enum  write_status {
	WRITE_OK = 0,
	WRITE_HEADER_ERROR,
	WRITE_DATA_ERROR,
	WRITE_PADDING_ERROR
};

enum write_status to_bmp(FILE* out, struct image* img);
#endif
