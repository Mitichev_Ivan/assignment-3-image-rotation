#ifndef READ_STATUS_H
#define READ_STATUS_H
#include "image.h"
#include <stdio.h>
enum read_status {
	READ_OK = 0,
	READ_INVALID_HEADER,
	READ_INVALID_PIXELS,
	READ_OUT_OF_MEMORY
};

enum read_status from_bmp(FILE* in, struct image* img);
#endif

